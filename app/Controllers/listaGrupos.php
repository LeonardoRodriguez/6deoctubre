<?php namespace App\Controllers;

use App\Models\GrupoModel;

class listaGrupos extends BaseController {
     public function index()
        {
            $data['titulo'] = "Listado de Artículos con findAll";
           //Instanciamos la clase 
            $datos = new GrupoModel();
           //solicitamos los datos al modelo 
            $data['grupos'] = $datos->findAll();
            //pasamos los datos a la vista
            echo view('comun/cabecera', $data);
            echo view('grupos/mostrar', $data);
            echo view('comun/pie');
        }

}


/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

